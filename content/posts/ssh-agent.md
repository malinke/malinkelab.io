---
Title: "Ssh Agent"
date: 2019-02-02T17:02:25+01:00
draft: false
Description: ""
Tags: []
Categories:
  - linux
  - ssh
---

I recently wanted to edit this blog from my RaspberryPi when I'm logged in via
my iPad. Since the raspbian on my RaspberryPi has no X environment installed
that could start an ssh-agent instance I needed another way. [This
answer](https://unix.stackexchange.com/a/390631/120121) on unix stackexchange
shows how to use systemd.

First I have to create a service file `~/.config/systemd/user/ssh-agent.service`

```systemd
[Unit]
Description=SSH key agent

[Service]
Type=simple
Environment=SSH_AUTH_SOCK=%t/ssh-agent.socket
ExecStart=/usr/bin/ssh-agent -D -a $SSH_AUTH_SOCK

[Install]
WantedBy=default.target
```

and add the following to `~/.pam_environment`

```bash
SSH_AUTH_SOCK DEFAULT="${XDG_RUNTIME_DIR}/ssh-agent.socket"
```

After enabling and starting the service I have a running ssh-agent service.
Systemd also only starts a single ssh-agent instance. Now I can generate a
keypair for gitlab on the RaspberryPi and use this to access my repositories.

If you do not want to use systemd here is an
[alternative](http://mah.everybody.org/docs/ssh) to start ssh-agent when you
login to shell.
