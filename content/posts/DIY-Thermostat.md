---
Title: "DIY Thermostat"
date: 2019-02-03T22:09:56Z
Description: ""
Tags: [RoboGardener]
Categories: []
---

My wife and I enjoy cooking a lot. We had the wish to add fresh herbs to our
cooking for a while. However, we are notoriously bad with plants. We have the
opposite of a green thumb. Tonfix these unfortunate circumstances I recently had
the idea to use the old RaspberryPi I have lying to build a RoboGardener to take
care of our herbs. Other people already had the same idea.

[Instead](https://www.cyber-omelette.com/2017/09/automated-plant-watering.html) [of](https://medium.com/@albertolamana/water-plants-with-raspberry-pi-9a1127b57b99) [using](http://www.esologic.com/piplanter-a-plant-growth-automator/) [an](https://blog.serverdensity.com/automatically-watering-your-plants-with-sensors-a-pi-and-webhooks/) [existing](https://www.techradar.com/how-to/computing/how-to-automatically-water-your-plants-with-the-raspberry-pi-1315059) [RoboGardener](https://mtlynch.io/greenpithumb/) [project](https://www.hackster.io/ben-eagan/raspberry-pi-automated-plant-watering-with-website-8af2dc) I thought it would be a good
idea to start from scratch to learn about new technologies. My idea is to write
the Gardener program in golang with a REST API to build a website that shows the
state of my garden. This way I have an excuse to learn go and get to know web
development.

As a start of this project I bought a DHT22 sensor. The sensor can read
temperature and humidity, and the bundle on
[Amazon](https://www.amazon.de/gp/product/B078SVZB1X/) came with a PCB that
required no slodering. As a prototype I want to build a website that can query
the sensor and show me the current temperature in my flat. Bonus points would be
the ability to render a history of the temperature.

The project will be described in a series of blog posts.

# DHT22 Sensor and Golang

The DHT22 sensor can be read using a single pin interface. [Last Minute
Engineers](https://lastminuteengineers.com/dht11-dht22-arduino-tutorial/) has a
good description of the sensor and it's communication protocol.

I started looking into reading the sensor from golang. There are currently two
sokutions available. The [go-dht](https://github.com/d2r2/go-dht) library and
the [embd](http://embd.kidoman.io/) library. go-dht has a convenient API to use.
For performance reasons the actual logic is implemented in C. Embd is a generic
GPIO library in pure go. Besides the RaspberryPi it also supports a range of
other SBCs. There is a [small
project](https://github.com/morus12/dht22/blob/master/dht22.go) with code I can
use to test if a pure go library is usable for me on a raspberrypi B+.
Unfortnately on my raspberry pi a pure go solution is not fast enough. I always
end in an infinite loop trying to read the sensor answer. Therefore I have to
use the go-dht library.



