---
title: Reading emails inside of emacs
date: "2017-08-13"
description: "Setting up ISync and mu4e in emacs"
categories:
  - "org"
  - "email"
  - "emacs"
  - "mu4e"
---

This is a document recording how I can setup a system for receiving email via
CLI and reading it in emacs.


# Receiving Email

There are several ways to do this. Most tools don't really talk with IMAP
servers though and aren't able to sync back to them. This is not really working
for me since I also want to be able to read my email on my phone. There are two
tools for this 'offlineimap' and 'isync'.


## isync

Lets start with isync. isync is my tool of choice for syncing. I'm using it
on my backup server at home to have a copy of my mail. The setup there is run
in a uni directional master->slave sync. This is really nice since any
changes on the backup server won't propagate to the imap repository.
Additionally it is also nice to run on my laptop.


### Config

This the config for a normal imap repository using an ssl connection

    # configure an account
    IMAPAccount   name
    Host          provider
    User          email
    Pass          password in clear test
    SSLType       IMAPS
    
    # configure the imap store
    IMAPStore     name-cloud
    Account       name
    
    # configure the local maildir
    MaildirStore  name-maildir
    # trailing slash is important!
    Path          /home/mail/
    Inbox         /home/mail/Inbox
    # this is needed to also download sub folders
    Flatten       .
    
    # combine everything
    Channel   name
    Master    :name-cloud:
    Slave     :name-maildir:
    Create    Slave
    Expunge   Slave
    # bi directional sync
    Patterns  *
    SyncState *


### SSL

To add custom SSL certificates add the following line to the `IMAPAccount`
section.

    CertificateFile  /path/to/cert.pem
    SystemCertificates no


### GMail

Mainly the Imap account has to be set to `gmail`. It will down

    IMAPAccount      gmail
    Host             imap.gmail.com
    User             account@gmail.com
    Pass             hahanotmypassword
    SSLType          IMAPS
    
    IMAPStore  gmail-cloud
    Account    gmail
    
    MaildirStore  gmail-maildir
    Path          /home/user/mail/maildirs/gmail/
    Inbox         /home/user/mail/maildirs/gmail/Inbox
    Flatten       .
    
    Channel   gmail
    Master    :gmail-cloud:
    Slave     :gmail-maildir:
    Create    Slave
    Expunge   Slave
    # Exclude everything under the internal [Gmail] folder, except archived mails
    Patterns * ![Gmail]* "[Gmail]/Sent Mail" "[Gmail]/All Mail" "[Gmail]/Drafts" "[Gmail]/Trash"
    SyncState *


## offlineimap

Offlineimap is a common tool used to sync mails with a imap repository. It
can only do a two way sync with the IMAP server and it is possible to add
arbitrary python code to the config-file. This is a huge flexibility which
allows to store my passwords in the gnome-keyring, huge gain in convenience.
Passwords can also be stored in other encrypted formats. It is also very nice
that offlineimap uses the Maildir format for it's local backends.


### Config

offlineimap ships with an excellent example configuration to start from.
Check you distributions package manager to see where it is located.


### Password Storage

Below is the script I use to save and retrieve the passwords from the
gnome-keyring.

```python
    #! /usr/bin/env python2
    
    import gnomekeyring as gkey
    import argparse
    
    
    def parse_args():
        p = argparse.ArgumentParser()
        p.add_argument('repository', type=str,
                    help='Account Name')
        p.add_argument('username', type=str,
                    help='User Name')
        return p.parse_args()
    
    
    def set_credentials(repo, user, pw):
        KEYRING_NAME = 'offlineimap'
        attrs = {'repo': repo, 'user': user}
        keyring = gkey.get_default_keyring_sync()
        gkey.item_create_sync(keyring, gkey.ITEM_NETWORK_PASSWORD,
                            KEYRING_NAME, attrs, pw, True)
    
    
    def get_credentials(repo):
        gkey.get_default_keyring_sync()
        items = gkey.find_items_sync(gkey.ITEM_NETWORK_PASSWORD,
                                    {'repo': repo})
        return items[0].attributes['user'], items[0].secret
    
    
    def get_username(repo):
        return get_credentials(repo)[0]
    
    
    def get_password(repo):
        return get_credentials(repo)[1]
    
    if __name__ == '__main__':
        import getpass
        args = parse_args()
        password = getpass.getpass(
            'Enter password for user "{}": '.format(args.username))
        password_confirmation = getpass.getpass('Confirm password: ')
        if password == password_confirmation:
            set_credentials(args.repository, args.username, password)
        else:
            print('Error: password confirmation does not match')
```

This is based on a script found in the ARCH-wiki. To enter the passwords call
the script with \`./.offlineimap.py <reponame> <username>\`. To use the script
in offlineimap add the following to the '[general]' section to the
offlineimap configuration.

```toml
    [general]
    pythonfile = ~/.offlineimap.py
```

Accessing this in the remote config can be done via. Here repo-name must be
the same that you have used before to set the password with '.offlineimap.py'

```toml
    [Repository remote-repository]
    type = IMAP
    remoteusereval = get_username("repo-name")
    remotepasseval = get_password("repo-name")
```


### SSL-Setup

since 2015 offlineimap uses SSL encrypted connections by default. This is
mostly fine since I can just point it to
'/etc/ssl/certs/ca-certificates.crt'. This should work for most major email
providers. In case it doesn't work you will need to find ssh certificate of
your provider.


### GMail

GMail accounts are a little bit special in the sense that they talk IMAP but
store email not in folders. It is possible to sync up the different labels
as individual folders though. All that is needed is to specify with GMail as
the IMAP server protocol.


# Reading Email in Emacs

There is a huge variety of programs to read mail locally from Maildir folders.
Even most Graphical Interfaces can do it like 'ClawsMail', 'KMail' and
'Evolution'. But I would like to stay with a pure test based option for the
time being. Also here exists a lot of options. The most commonly know is
likely 'mutt'. But there are all sorts of others like 'sup', 'luamail2' and
'alpine'. Around 2010 programs started to appear that specialize search and
filtering mail. The two most prominent here are 'mu' and 'notmuch'. They are
designed to be used by other programs as a mail searcher. If you google for
your favorite CLI mail programm and either of them you will surely find a lot
of information how to use them. I want to use emacs as my main email reader
and both programs have emacs frontends. But later I will only describe how to
setup mu and it's emcas frontend mu4e


## mu4e

mu has an excellent emacs frontend called mu4e, mu for emacs. There already
exists a good manual on the mu4e website and usage is simple since a lot of
UI concepts of emacs are leveraged.

This text won't go into detail how to setup mu4e for yourself since that is a
personal thing in any regard it will just list the stuff I found interesting
for my self. As a default I use the spacemacs configuration of mu4e with
small changes. Below are all the mu4e setting I'm using.

```elisp
    (setq mu4e-maildir "~/mail/")
    ;; not using smtp-async yet
    ;; some of these variables will get overridden by the contexts
    
    (setq mu4e-get-mail-command "mbsync --all"
          mu4e-attachment-dir "~/Downloads"
          mu4e-view-show-images t
          ;; change filename to avoid duplicate UID in mbsync
          mu4e-change-filenames-when-moving t
          )
    (setq mu4e-context-policy 'pick-first)
    (setq message-kill-buffer-on-exit t)
```

Important is `mu4e-change-filenames-when-moving t`. If you don't set this
isync will be confused because it finds several messages twice with the same
identifier. This leads to a loss of email when syncing to the imap
repository.


### Using multiple accounts

To setup using several accounts like private email and work email, one can
use contexts. The idea is that based on your current maildir you set
different variables for mu4e. This can be your smtp server, email address,
signature and refilling options. The manual as a good example how to use contexts.


### Refilling

In other mails-clients this would be called filtering. The normal filtering
approach is not very good controllable and can't hardly be checked for
errors before executing it. This is not the case for mu4e here refilling is
a two step process. First you tell mu4e which emails you want to refile,
check to where they will be refilled to and only then execute. But that is
not the cool thing about refiling in mu4e. The actually cool thing is that
one can use elisp as a language to decide where a mail has to be refilled
to. That gives infinitely more power then just a simple rule based
filtering. This is super useful for me. I have a few emails I want to place
into specific buckets like amazon emails. Below is a refile setup to filter
messages from amazon and move everything else into a yearly archive folder.

```elisp
    (mu4e-refile-folder . (lambda (msg)
                            (cond
                             ((mu4e-message-contact-field-matches msg :to "@amazon.")
                              "/E-Commerce.Amazon")
                             (t (concat "/Archives." (format-time-string "%Y"))))))
```


## notmuch vs mu

notmuch and mu are in principle very similar. They both use xapian as a
database backend and enable searching your mail very very fast. The
difference between the two is that mu only searches your mail while notmuch
is also able to add different tags to your mail, similar to gmail labels.

Personally I like the approach of looking for email by just searching for it.
With the normal folder approach used in basically every email programs that
exists I always had the problem of deciding where to place the mail.
Searching then in return required me to remember into which folder I had
moved it. mu completely frees me of that. To look at my email on the phone
though it is nice to have a limited set of folders to search for recent
messages according to a topic. These topics are very general like
'E-commerce' for things like amazon, kickstarter, etc and service-provider
for my electrical company, steam and the likes.


# Sending mail with Emacs

To send email I use the smtp package in emacs. This stores passwords in an
encrypted authinfo file.

```elisp
    (setq
     send-mail-function 'smtpmail-send-it
     message-send-mail-function 'smtpmail-send-it
     smtpmail-smtp-server "smtp.fastmail.com"
     smtpmail-smtp-service 465
     smtpmail-stream-type 'ssl
     )
    (setq auth-sources (quote ("~/.authinfo.gpg" "~/.netrc")))
```

