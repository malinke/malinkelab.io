---
title: Setting up a home backup server
date: "2016-10-24"
description: "Cheap and quiet offline automated home backup"
categories:
  - "email"
  - "backup"
---

Since I'm married I started to take more pictures and also take digital copies
of our contracts and important documents. Then there is also my music collection
that I would like to avoid having to rip from my CDs again in case something
happens to my laptop. Because of this I've started looking into a cheap solution
to have automated backups run at home. The top requirement for the backups is
that they have to be setup to run automatically without my intervention. I
strongly believe this is the only viable option to make regular backups. Because
of this I decided against just an external hard drive with a rsync-script.


# What server to use

Since the server would have to say in the bedroom I was looking for something
small and quiet. Ideally the cost of the whole setup wouldn't cost more then
400€. The intel NUC machines fit perfectly into that corner. You can get a
recent i3 CPU in a small form factor (10x10x5 cm) for about 270€, Pentium and
Celeron CPU's are even cheaper. Every NUC comes without RAM and a harddrive.
I've decided for a NUC6i3SYH with a recent i3 and bought 8 GB of RAM, a 1.5 TB
samsung hard drive and a 64 GB Transcend SSD. The whole setup cost me 400.60€.
Selecting a Pentium or Celeron CPU I could have saved 50 - 100€ but I like the
idea to have some spare CPU power to later run some other services on the
machine as well.


# Operating System

For an operating system I went for debian stable. Debian is a good choice for
me since it offers relative up to date software for a server OS (compare
against RHEL) with a decent security update policy. Additionally almost all
the software I need will be packaged in the distro repositories. This means I
can setup automated updates and only need to do some regular checkups what
updates have been installed. In case I would need recent versions of some
software aptitude makes it also very easy with pinning to select only some
packages from debian testing while most of the system still runs stable. This
for example will allow me to use a recent kernel without problems.


# Quiet The HDD

Since silence is an important proberty of the system the HDD should be
completely stopped when ever possible. The easiest way to achieve this is to
install laptop-mode and configurate it to enable all energy saving options
also when the system is on AC power. But you might notice that your HDD won't
spin down after you created and mounted a new ext4 partition on it. This is
likely because you are lazy initializing the inodes, you can check this with
iotop. This system is thought of as a convenience to allow you directly to use
the partition. The lazy initialization can last days for large drives because
the kernel tries to only write small partitions at a time to not interfere
with normal usage. But instead you can also directly initialize all inodes
when you create the partition with

```bash
mkfs.ext4 -E lazy_itable_init=-1 /dev/sda1
```


# Automated Backup

To have automated backups from any Linux machine it is enough to have a ssh
running on the server. With that one can use rsync and systemd on the target
machine to setup an automated system. See previous post about automated
backups with system and rsync.


# Plans for making cloud backups

The server I have right now still has a number of scenarios where I could
loose all my data. That is pretty much any instance where something happens to
the hard drive of the server unnoticed. To eliminate that risk I will have to
make a copy of my data to a service like dropbox or amazon glacier. The
problem with this is of course that I have to assume anything I upload to them
will be read by third parties. So extra steps are required to secure my data.
I haven't looked into to many options now. But **git annex** looks like a good
solution to have an extra copy on another machine. I can use it to setup
several copies and all copies are only stored encrypted with pgp. That might
be a promising solution to eliminate the risk of hardware failure of my
server.

