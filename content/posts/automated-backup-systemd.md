---
title: automated backups with systemd and rsync
date: "2016-09-01"
description: "Have systemd backup your work"
categories:
  - "systemd"
  - "backup"
---

I do most of my research and development on my laptop. Internally the laptop has
no backup besides some version control of my code. This isn't ideal and I was
looking for a automated solution that would backup my data every time I connect
my laptop at home or at work.

For the backup itself I can use a simple rsync script. To execute the backup at
specific intervals I'm using systemd with user specific units, this is nice
since it works without root and I can use my user account ssh-keys to connect to
the remote machine. Systemd also allows me to add a precondition that must be
met before the actual backup is started, I'm using that to ensure the backup
machine is reachable.

This is the service unit `~/.config/systemd/user/backup.service`:

```systemd
    [Unit]
    Description=Backup Work Related Files
    
    [Service]
    Type=oneshot
    ExecStartPre=/usr/bin/ping -c 1 -W 1 xxx.xxx.xxx.xxx
    ExecStart=/usr/bin/rsync -avzuhH --progress --delete --exclude=.Trash-1000 /mnt/work-stuff/ user@xxx.xxx.xxx:Documents/backup-notebook
```

Here I use `ExecStartpre` and ping to ensure that the backup machine is
reachable. This works because ping returns an error if it doesn't receive an
answer and systemd stops the whole unit if any command fails. For more
information about the unit script have a look at this excellent [post](http://hokstadconsulting.com/devops/writing-systemd-units).

You can check if the unit was found with `systemctl --user list-unit-files` and
run it with `systemctl --user start backup`. In case you had some typo or want
to add more folders to the backup you have to reload the systemd daemon with
`systemctl --user daemon-reload`. More directories can easily be added with
another `ExecStart` in the `[Service]` section. Because we declared the unit as
a `oneshot` systemd will execute all script defined in `ExecStart` in the order
they are declared. To have a look at the files rsync is copying and if the
service is working correctly use `journalctl --user-unit=backup_work.service`.

Now to the timer `~/.config/systemd/user/backup.timer`:

```systemd
    [Unit]
    Description=Run bi-hourly backup
    
    [Timer]
    OnCalendar= *-*-* 00/2:00:00
    
    [Install]
    WantedBy=timers.target
```

It is important that the timer unit has the same name as the service unit. If
you don't want to do that you can also specifically tell systemd which unit it
should run. To find out more about user defined timer check out this [post](https://major.io/2015/02/11/rotate-gnome-3s-wallpaper-systemd-user-units-timers/).

The timer can be activated with `systemctl --user start backup.timer` or to
enable it across reboots `systemctl --user enable backup.timer`. If you want to
check when the timer is executed the next time use `systemctl --user
list-timers`.

Setting all of this up has been surprisingly easy. I really enjoy that I'm also
able to see when the backup is executed the next time and that I have an easy
way to query the logs of the backup that works universally with **journalctl**.

