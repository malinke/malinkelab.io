---
title: ClusterSSH
date: "2016-09-10"
description: "Manage several hosts without a lot of fuss"
categories:
  - "administration"
---

At work I'm managing a small collection of workstations in addition to what I'm
actually paid for. This means I can only spend very little time on
administrating those machines and that any tool helping me with that task should
be usable within minutes. So tools like puppet, ansible and chef are not goind
to work, while they might be nice they require to much work from me upfront to
get working and get comfortable with.

[**clusterssh**](https://github.com/duncs/clusterssh) is an easy to use tool that lets me login on several machines at
the same time and execute the same commands on them. It works by opening ssh
connections to a list of hosts and showing each connection in a X-window. This
allows me to install packages simultaneously on several machines while I can
also check that each machine does the right thing. That is a huge time saver and
immediately usable, exactly what I was looking for.

To connect to two hosts use `cssh user@host1 user@host2`. It also works with
aliases defined in `~/.ssh/config` and it is possible to create predefined
groups of hosts in `~/.clusterssh/clusters`.

With version 4.07 the definition of cluster is `groupname host1 host2 ...`.
Notice that the first word is the groupname and then the list of hosts is space
separated. This differs from the examples for older versions of clusterssh that
can be found on the internet.

