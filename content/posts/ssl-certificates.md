---
title: Testing a SSL connection
date: "2016-10-19"
description: "using openssl to test a ssl connection"
categories:
  - "email"
  - "openssl"
---

I currently want to switch from thunderbird to a emacs as my email client. Part
of this transition is to use a tool like **isync** or **offlineimap** to sync my
mail to be offline accessible. Both tools allow to connect to the imapclient
using SSL. Setting this up for commercial provider like gmail isn't a problem
and there are tons of tutorials on the internet. But I had problem getting ssl
to work with my work email. Here I explain how one can use `openssl` to find out
what certificates are missing in a certificate chain and how to test if the
final connection works. The explanations are short and likely won't help much by
them self but they are the missing pieces I didn't found easily on the web.


# Testing a Connection

To test the IMAP connection to the domain **whereever.com** run

`openssl s_client -connect www.whereever.com:993`

The port 993 is used by the imap protocol but you can also use any other
port. If you don't have all certificates needed to verify your connection in
the standard ca-installations you can also specify it adding `-CAfile
  /where/the/ca/is/stored.crt`.


# Extracting missing certs from certificate chain

To have openssl show you the certificates for a connection use

`openssl s_client -connect www.whereever.com:993 showcerts`

That prints the whole certificate chain send by the server. This chain can be
used to create a valid `crt` file to manually add trust this server. First you
want to copy everything that is in enclosed in **BEGIN CERTIFICATE** and **END
CERTIFICATE** into a `*.crt` file. This is likely not the entire certificate
chain. The root certificate can either come from a trusted Certificate
Authority (root CA), whose certificates can be installed with the
ca-certificates package on most distributions. But you might also lack some
other certificates in the chain. In that case you have to ask the person
running the server you are connecting to for the certificates.


# Find which root CA is needed

The first cert printing with `showcerts` will tell you the name of the root CA
that was used to sign the chain. To get the public cert file you can now
google if to find the public cert from the CA-authority or you can also check
if the support of the domain you are connecting to provides a `crt` file.

