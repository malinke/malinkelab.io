---
title: Use gnome-keyring for ssh key management with systemd jobs
date: "2017-02-16"
description: "Use gnome-keyring for systemd with i3"
categories:
  - "systemd"
  - "backup"
  - "ssh"
---

I recently started to use i3 as my window manager. I really like the tiling
management for windows. But this also meant that I wouldn't have my
gnome-keyring to unlock my ssh-keys without some configuration.


# Use gnome-keyring with i3

To unlock the keyring I start it in my `~/.profile` with the following.

```bash
# start gnome keyring everytime
if [ "$0" = "/etc/X11/xinit/Xsession" -a "$DESKTOP_SESSION" = "i3" ]; then
    export $(gnome-keyring-daemon --start --components=ssh)
fi
```

I'm using lightdm as a login manager and it's starting a X-session with
`/etc/X11/xinit/Xsession`. If you want to find out the session of your login
manager add the following to your `~/.profile`

```bash
## debug keyring unlock
LOG="$HOME/profile-invotations"
echo "-----" >>$LOG
echo "Caller: $0" >>$LOG
echo "DESKTOP_SESSION: $DESKTOP_SESSION" >>$LOG
echo "GDMSESSION: $GDMSESSION" >>$LOG
```


# Use gnome-keyring for systemd backups

One issue I encountered with this setup is that my backup jobs that I defined
with systemd would ask me about the password for my keys. With gnome this
never happened. After some research and trial and error it turns out that the
gnome-keyring takes over `ssh-add` and also sets the `SSH_AUTH_SOCK`
environment variable. But this environment variable isn't available during the
systemd job. To make it available I write a environment variable file at the
end of my i3 config with.

```bash
exec --no-startup-id echo "SSH_AUTH_SOCK=$SSH_AUTH_SOCK" > ~/.cache/ssh-systemd-envfile
```

Now I can load that file in the `[Service]` section of the systemd service

```systemd
EnvironmentFile=/home/max/.cache/ssh-systemd-envfile
```

This way I never get asked for my password for the key anymore and the backup
is done in the background without my intervention.

