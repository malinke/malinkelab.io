---
title: Sharing array data between julia and python
date: "2017-11-29"
description: "Can I efficiently transfer data from julia to python"
categories:
  - "julia"
  - "python"
---

I recently started writing julia code for some Monte-Carlo simulations. One
problem though are the plotting libraries available in julia. While they are
very good they cannot compete with matplotlib currently IMHO and it's hard to
generate plots that look like matplotlib to ensure a consistent look of plots
for a paper. Therefore I need a way to share results calculated in julia with
python to plot them. This is thankfully extremely easy using [HDF5](https://support.hdfgroup.org/HDF5/) files. They
can be written in julia with the [HDF5](https://github.com/JuliaIO/HDF5.jl) package:

```julia
using HDF5

a = collect(reshape(1:120, 15, 8))
h5write("test.h5", "agroup", a)
```

Here we stored the array `a` in a hdf5 file called `test.h5`. The file is stored
in a group called `agroup`. With groups it is possible to share several
logically sorted arrays in a single file.

On the python side I can use [h5py](http://docs.h5py.org/en/latest/quick.html#quick) to read the array into numpy:

```python
import h5py
import numpy as np

f = h5py.File('test.h5')
agroup = f['agroup']
a = np.asarray(agroup['a']).T
```

Note here that I transpose the array `a` that is read from the file. This is
because julia and numpy use different in memory layouts for arrays. While julia
is using the column-major order numpy is using a row-major order. This means
that we have to use the transpose to get the same arrays back. Please note that
this can have performance affects for very large arrays.

So that's all that is needed to exchange numerical data between julia and python.

