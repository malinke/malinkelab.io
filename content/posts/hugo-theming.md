---
Title: "Hugo Theming"
date: 2019-02-01T23:42:53+01:00
Description: ""
Tags: []
Categories: []
---

I planned to do some projects with a raspberry pi at home soon. The projects
involve reading some sensors and displaying their values on various devices and
operating systems. As I do not want to write several native app I figure a
webpage is ideal to have a consistent UI across several OSes.

For this however I have to learn actual web development. To get started I
decided to redo my blog and build my own theme. For the beginning I used the
tutorial for the [blank
theme](https://themes.gohugo.io//theme/blank/post/creating-a-new-theme/). It is
very bare bones gut forces me to learn HTML and Hugo. I assume the website I
build for the raspberry pi attached sensors will also be build with hugo.
