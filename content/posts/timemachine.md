---
title: Timemachine backups using Linux
date: "2016-11-27"
description: "Backup a Mac using netatalk"
categories:
  - "backup"
---

I've already setup a automated backup for my own machine and now I wanted to
have a similar automated backup for my wife's mac. Since it should be easy for
her to recover previous versions of files I looked if it is possible to use
timemachine with the debian server I'm using for my backups. There is a tool for
linux called [netatalk](http://netatalk.sourceforge.net/) to interact with macOS and it allows my debian machine to
look like a timemachine to macOS.


# Build deb of netatalk 3.x.x

I'm using debian 8 on my server. Unfortunately due to licensing issues debian
isn't packaging the current version of netatalk right now. Luckily there is an
unofficial package on [github](https://github.com/adiknoth/netatalk-debian). You will still have to build the package
yourself though but using the github repo that is easy. To instal the build
dependencies run:

```bash
sudo aptitude install build-essential devscripts debhelper cdbs autotools-dev dh-buildinfo libdb-dev libwrap0-dev libpam0g-dev libcups2-dev libkrb5-dev libltdl3-dev libgcrypt11-dev libcrack2-dev libavahi-client-dev libldap2-dev libacl1-dev libevent-dev d-shlibs dh-systemd
```

After you cloned the repository build the package with inside of the
repository.

```bash
debuild -b -uc -us
cd ..
```

There should be two deb packages now. `libatalk*deb` and `netatalk*deb`
install both with.

```bash
sudo dpkg -i libatalk16_*.deb
sudo dpkg -i netatalk_*.deb
```

You also have to install the avahi daemon.

```bash
sudo aptitude install avahi-daemon libc6-dev libnss-mdns
```


# Settings

First you have to create a backup directory. Then you need to add the following to `/etc/netatalk/afp.conf`

```toml
    [TimeMachine]
    path = /path/to/backup
    time machine = yes
    vol size limit = 500000
    valid users = username
```

The volume limit is in kilobyte. For the user you can also create a new one
only for the timemachine backup.


# Mac OS setup

Run `defaults write com.apple.systempreferences
  TMShowUnsupportedNetworkVolumes 1` to enable non-Apple timemachine network
drives. Afterwards you should be able to select the setup netatalk machine and
login with the account name of the specified valid user.


# Sources

<https://samuelhewitt.com/blog/2015-09-12-debian-linux-server-mac-os-time-machine-backups-how-to>

