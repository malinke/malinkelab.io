---
title: Arch Linux
date: "2017-08-15"
description: "Using Arch linux"
categories:
  - "admin"
  - "arch"
  - "linux"
---

I recently reinstalled my laptop because I was starting to be annoyed with
fedora. The wiki is pretty awesome but since it spreads information a little bit
I'm listing the steps again here in the order I applied them.


# Base Installation

For partioning I reused the LVM setup from fedora. I just need to check that I
mount the partitions correctly before boostrapping arch. Noticeably I can't
mount the swap partition now. This has to be done when I'm in the new system
with a chroot. More on that later.

```bash
    mount /dev/mapper/fedora-root /mnt
    mkdir /mnt/home /mnt/boot
    mount /dev/mapper/fedora-home /mnt/home
    mount /dev/sda1 /mnt/boot
```

Now I can boostrap a base system. I'm already installing the basic devel
packages at this stage.

```bash
    pacstrap /mnt base base-devel
```

Now I can generate the fstab file and actiavte the swap partition

```bash
    genfstab -U /mnt >> /mnt/etc/fstab
    # find swap partition UUID
    lsblk -o UUID /dev/mapper/fedora-swap
```

Afterwards I can manually add the swap partition like this.

```bash
    # /dev/mapper/fedora-swap
    UUID=1598cfed-a7e3-4c9c-80a7-7ef40bc3032d       none            swap            defaults                        0 0
```

The other settings for locale and hostname can be looked up on the wiki. I
followed the guide as given. For the network management I installed
networkmanager and activated the systemd service.

Using LVM though means I have to change the initial ramdisk and rebuild it with
`mkinitcpio -p linux`. You have to use the following line to enable LVM for
grub. The file contains comments explainint that.

```bash
    # /etc/mkinitcpio.conf
    HOOKS="base udev autodetect modconf block lvm2 filesystems keyboard fsck"
```

As bootloader I'm going with grub. Here the LVM setup requires manual changes to
the grub default configuration. You have to add the `root=` kernel parameter
before generating your grub config.

```bash
    # /etc/default/grub
    GRUB_CMDLINE_LINUX_DEFAULT="quiet root=/dev/mapper/fedora-root"
```

After this I got a fully working system with root account and internet that I
can boot into again.


# Gnome Installation

Before installing gnome I wanted to have a unpreviliged user besides root to use.

```bash
    useradd -m -G wheel -s /bin/bash max
    passwd max
```

Afterwards I installed sudo to not have to login to root all the time. But my
user isn't allowed to use sudo yet. First you have to use `visudo` to allow the
group wheel to use sudo.

Afterwards I can install gnome and gdm to have a GUI. This will also magically
install everything to enable wifi and key management.

```bash
    pacman -S gnome gnome-extra gdm
    systemctl enable gdm.service
```

Reboot and you will have a nice clean new gnome 3 installatoin.


# Arch User Repository AUR

Arch has a fairly hugh repository of packages but not everything is available.
But theu have a collection of packing files in the AUR. They can be downloaded
manually and installed. A better alternative is to use a yaourt or any other AUR
helper to install packages. yaourt is the most used. The installation is nice.

```bash
    yaourt package_name
```

This will search for the package name and give you a list of candidates to
install. When the package is from the AUR it will over you to have a look at the
packaging file before building and installing a package.


# Backup

backintime now apparently can do incremental backups over ssh. This will replace
my old setup with a simple rsync command and systemd timers.

