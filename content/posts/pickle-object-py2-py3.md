---
title: Share python pickles from python 2 to 3 and back
date: "2017-08-25"
description: ""
categories:
  - "python"
---

Recently I started using python 3 on my main laptop. But on the high performance
clusters I regularly do computations on I'm still using python 2.7. I do the
last steps of any analysis on my computer. This requires me to move the data
from the cluster to my laptop. Most of the times this is limited to simple numpy
arrays or pandas dataframes. Both of them can easily be read and written between
python versions. Occasionally though I use custom classes that I have to pickle.
When you stick with one python version pickles are awesome.

Reading and writing pickles between python 2 and 3 can be a problem. The first
issue is that python 3 has a higher pickle protokol. The highest version that
python 2.7 supports is **2**. With that knowledge we can now write and store our
data efficiently so it can be read from python 2 and 3.

```python
from six.moves import cPickle

# Writing
with open('file.pkl', 'wb') as fh:
    cPickle.dump(data, protocol=2)

# Reading
with open('file.pkl', 'rb') as fh:
    data = cPickle.load(fh)
```

I should note that this only works if you have python 3.4 or newer. This is
because python 3.0 to 3.3 had a bug reading python2.7 pickled classes since the
internal representation for a class has changed.

It can still happen to that classes pickled with python 2.7 that contain numpy
arrays aren't readable in python 3 because of a unicode error. I'm not sure why
this error exists but the solution is to add `__getstate__` and `__setstate__`
functions to your class to have exact control how a class is pickled.

```python
class Data(object):
    def __init__(self, awesome, science):
        self.awesome = awesome
        self.science = science

    def __getstate__(self):
        return [self.awesome.tolist(), self.science.tolist()]

    def __setstate__(self, state):
        self.awesome = state[0]
        self.science = state[1]
```

Using a python list/tuple in the state worked fine for me and should yield the
most robust results.

