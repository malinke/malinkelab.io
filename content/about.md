+++
title = "About"
description = "about this blog"
date = "2019-02-01"
slug = "About"
+++

## Blog

I use this blog as a personal knowledge base. I'm happy if the any of my
remblings help someone else. The style of the blog is very simplistic on purpose
and helps me to learn more about writing HTML.

**Gitlab**: https://gitlab.com/malinke

**Github**: https://github.com/kain88-de

