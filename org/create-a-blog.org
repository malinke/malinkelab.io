#+BEGIN_EXPORT html
---
title: How to create a blog
date: "2016-08-06"
description: "what does this do"
categories:
  - "org"
  - "hugo"
  - "emacs"
---
#+END_EXPORT

This blog is mostly thought as a personal archive for myself to remember tricks
and have some own docs for different programs I use.

I start with the first to document how to setup this blog.

* Writing posts in org-mode

  Since I like writing all my text in [[http://www.orgmode.org][org-mode]] I also want to write this blog
  with it. There is a small problem though that [[https://gohugo.io][hugo]], the static website engine,
  uses markdown as a format for it's blog posts. But orgmode has a solution for
  this. I can export the posts I'm writing to markdown. For this I'm using the
  publishing function in orgmode. It will convert all =*.org= files I have in
  the org subfolder to markdown files that are used by hugo to generate the
  html. To setup org-publish to handle all conversion you have to add the
  following to your emacs initialization.

  #+BEGIN_SRC elisp
    (setq org-publish-project-alist
          '(("orgfiles"
             :base-directory "~/foss/blog/org"
             :base-extension "org"
             :publishing-directory "~/foss/blog/content/posts"
             :publishing-function org-md-publish-to-md
             :section-number nil
             :with-toc nil)))
  #+END_SRC

  The hugo specific tags at the beginning of a post can be included with the
  following snippet at the start of a post.

  #+BEGIN_SRC org
  #+BEGIN_EXPORT html
  ---
  title: How to create a blog
  date: "2016-08-06"
  description: "what does this do"
  categories:
    - "org"
    - "hugo"
    - "emacs"
  ---
  #+END_EXPORT
  #+END_SRC

  That is about everything that needs to done besides a few changes to the
  layout to list me as the author of the blog and a license for the content. To
  make reuse of any content in this blog easy I chose *Creative Commons
  Attribution 4.0 International License*.

* Gitlab Pages

  The blog itself is hosted by [[https://pages.gitlab.io][gitlab pages]]. Gitlab is great for this because
  their solution is open source, free and uses https by default. It also makes
  the setup easy and allows me to choose any static website generator I want.
  This makes for an overall very nice experience.
